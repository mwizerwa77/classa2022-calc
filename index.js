module.exports.add = function (a, b) {
  return a + b;
};
module.exports.subtract = function (a, b, c) {
  return a - this.add(b, c);
};
module.exports.divide = function (a, b) {
  if (b == 0) {
    throw new Error("Divide by 0 is not possible");
  }
  return a / b;
};
